train = read.csv("train.csv")

train[,7:12] <- NULL

str(train) #allows you to see the classes of the variables (all numeric)

train[, 'PassengerId'] <- as.factor(train[, 'PassengerId'])
train[, 'Survived'] <- as.factor(train[, 'Survived'])
train[, 'Pclass'] <- as.factor(train[, 'Pclass'])
str(train)  